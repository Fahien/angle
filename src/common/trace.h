//
// Copyright 2021 The ANGLE Project Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//

// trace.h: Tracing utilities.

#ifndef COMMON_TRACE_H_
#define COMMON_TRACE_H_

#if defined(ANGLE_USE_PERFETTO)
#    include <perfetto.h>

#    define ENTRY_POINTS "entry-points"
#    define VALIDATION "validation"
#    define CONTEXT "context"
#    define STATE_TRACKER "state-tracker"
#    define BACKEND "backend"

PERFETTO_DEFINE_CATEGORIES(
    perfetto::Category(ENTRY_POINTS).SetDescription("Events associated to GL API entry-points"),
    perfetto::Category(VALIDATION).SetDescription("Events from the validation layer"),
    perfetto::Category(CONTEXT).SetDescription("Events from the ANGLE context"),
    perfetto::Category(STATE_TRACKER).SetDescription("Events from the ANGLE state-tracker"),
    perfetto::Category(BACKEND).SetDescription("Events from the ANGLE backend"));

#    define ANGLE_TRACE(c, e) TRACE_EVENT(c, e)
#    define ANGLE_TRACE_EP(f) ANGLE_TRACE(ENTRY_POINTS, f)
#    define ANGLE_TRACE_VAL(f) ANGLE_TRACE(VALIDATION, f)
#    define ANGLE_TRACE_CTX(f) ANGLE_TRACE(CONTEXT, f)
#    define ANGLE_TRACE_ST(f) ANGLE_TRACE(STATE_TRACKER, f)
#    define ANGLE_TRACE_BE(f) ANGLE_TRACE(BACKEND, f)
#else
#    define TRACE_EVENT_END(x)
#    define TRACE_EVENT_BEGIN(x, y)
#    define ENTRY_POINTS
#    define ANGLE_TRACE(c, e) (void)(c)
#    define ANGLE_TRACE_EP(f) (void)(f)
#    define ANGLE_TRACE_VAL(f) (void)(f)
#    define ANGLE_TRACE_CTX(f) (void)(f)
#    define ANGLE_TRACE_ST(f) (void)(f)
#    define ANGLE_TRACE_BE(f) (void)(f)
#endif



#endif  // COMMON_TRACE_H_
