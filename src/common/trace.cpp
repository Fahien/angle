//
// Copyright 2021 The ANGLE Project Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//

// trace.cpp: Tracing utilities.

#include "common/trace.h"

#if defined(ANGLE_USE_PERFETTO)
PERFETTO_TRACK_EVENT_STATIC_STORAGE();
#endif
